class AuctionChannel < ApplicationCable::Channel
  def subscribed
    stream_from "auction:bid"
  end

  def unsubscribed
  	stop_all_streams
  end
end
