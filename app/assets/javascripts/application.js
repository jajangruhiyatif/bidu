// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require cable.js
//= require jquery.min.js
//= require popper.min.js
//= require bootstrap.min.js
//= require Chart.min.js
//= require main-app.js
//= require toastr.min.js
//= require gauge.min.js
//= require moment.min.js
//= require daterangepicker.min.js
//= require jquery.dataTables.min.js
//= require slick.min.js
//= require select2.min.js
//= require sweetalert.min.js
//= require summernote.js

// require pace.min.js
// require dataTables.bootstrap4.min.js
// require main.js
// require ckeditor.js


$(document).ready(function() {
  $('.summernote').summernote();
});