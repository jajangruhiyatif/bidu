App.chats = App.cable.subscriptions.create("AuctionChannel", {
  connected: function() {
    // Called when the subscription is ready for use on the server
    console.log("connected");
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
    console.log("disconnected");
  },

  received: function(data) {
    // Called when there's incoming data on the websocket for this channel
    $(".top_bid_"+data.auction_id).html(data.top_price);
    $(".date_bid_"+data.auction_id).html(data.created_at);
    $(".bidder_count_"+data.auction_id).html(data.join_bidder);
    var content = "";
    content += '<div>'+
        '<div class="float-left"><i>'+data.created_at+'</i></div>'+
        '<div class="float-right">Rp '+data.top_price+'</div>'+
        '</div>';
    $(".log_bid_"+data.auction_id).prepend(content);
    /*$('#bid_btn_'+data.user_id).addClass("disabled");*/
    $('.label-top'+data.auction_id).addClass("d-none");
    $('.bid-btn'+data.auction_id).removeClass("disabled");

    toastr.options = {
        "preventDuplicates": false,
        "showDuration": "300",
        "hideDuration": "3000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var top_bid = $('.bid-btn'+data.auction_id).data('bidder');
    if(top_bid==data.user_id){
      $('.bid-btn'+data.auction_id).addClass("disabled");
      toastr.success("Anda telah menjadi top Bidder !");
      $('.label-top'+data.auction_id).removeClass("d-none");
    }else{
      toastr.info("User lain telah membuat bid baru.");
    }
  }
});
