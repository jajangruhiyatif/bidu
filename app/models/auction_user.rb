class AuctionUser < ApplicationRecord
	belongs_to :user, foreign_key: "user_id"
	belongs_to :auction, foreign_key: "auction_id"
end
