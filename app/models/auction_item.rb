class AuctionItem < ApplicationRecord
	belongs_to :user, :foreign_key  => "user_id"
	belongs_to :category
	belongs_to :brand
	belongs_to :brand_type
	has_many   :attachment
	has_one    :auction

	validates :name, presence: true
	validates :price, presence: true
	validates :description, presence: true
	validates :condition, presence: true

	def self.create_auction_item(params)
		user 			 = User.find(params[:user_id])
		category   = Category.find(params[:category_id])
		brand 		 = Brand.find(params[:brand_id])
		brand_type = BrandType.find(params[:brand_type_id])

		ai = AuctionItem.new
		ai.name 						= params[:name]
		ai.price 						= params[:price]
		ai.description 			= params[:description]
		ai.min_description 	= params[:min_description]
		ai.plus_description = params[:plus_description]
		ai.condition 				= params[:condition]
		ai.production_date	= params[:production_date]
		ai.tag 							= params[:tag]
		ai.user 						= user
		ai.category 				= category
		ai.brand 						= brand
		ai.brand_type 			= brand_type
		ai.save
		return ai;
	end
end
