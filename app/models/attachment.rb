class Attachment < ApplicationRecord
	belongs_to :AuctionItem, :foreign_key => "auction_item_id"

	validates :file_type, presence: true
	validates :name, presence: true

	has_attached_file :name, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :name, content_type: /\Aimage\/.*\z/
end
