class LogBid < ApplicationRecord
	belongs_to :auction
	belongs_to :user

	after_commit :reload_page

	def reload_page
		auction = Auction.find(self.auction_id)
		log_bid = auction.log_bid
    ActionCable.server.broadcast "auction:bid", {
    	top_price: log_bid.order(price: :asc).last.price,
    	auction_id: self.auction_id,
    	user_id: self.user_id,
    	join_bidder: auction.auction_user.count,
    	created_at: log_bid.order(price: :asc).last.created_at.strftime("%Y-%m-%d %H:%M")
    }
  end

	def self.create_bid(auction_id, bidder_id, bid_raise)
		ActiveRecord::Base.transaction do
			bid = LogBid.new
			bid.auction_id = auction_id
			bid.user_id = bidder_id
			bid.price = bid_raise
			bid.save

			if bid.save
					AuctionUser.find_or_create_by(user_id: bidder_id, auction_id: auction_id) do |auction_user|
						auction_user.status = 'join'
					end
				return bid
			else
				ActiveRecord::Rollback
				return false
			end
		end
	end
end
