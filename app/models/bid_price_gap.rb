class BidPriceGap < ApplicationRecord
	validates :upper_limit, presence: true
	validates :lower_limit, presence: true
	validates :price, presence: true
end
