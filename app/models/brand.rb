class Brand < ApplicationRecord
	has_many :brand_type, dependent: :destroy
	has_many :auction_item, dependent: :nullify
	belongs_to :category
end
