class PersonalInformation < ApplicationRecord
	belongs_to :user, :foreign_key  => "user_id"

	validates :first_name, presence: true
	# validates :phone_number, presence: true
	# validates :address, presence: true
	# validates :gender, presence: true

	has_attached_file :ktp, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :ktp, content_type: /\Aimage\/.*\z/

  has_attached_file :profile_picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :profile_picture, content_type: /\Aimage\/.*\z/

	def self.create_personal_information(params)
		byebug
		user = User.new
		user.username 		= params[:username]
		user.email			  = params[:email]
		user.phone        = params[:phone_number]
		user.password 		= params[:password]
		user.is_active 		= true

		if user.save
			pi = PersonalInformation.new
			pi.first_name 				= params[:first_name]
			pi.last_name 		  		= params[:last_name]
			pi.phone_number	  		= params[:phone_number]
			pi.birth_place	  		= params[:birth_place]
			pi.birth_date	     		= params[:birth_date]
			pi.telephone	        = params[:telephone]
			pi.profile_picture	  = params[:profile_picture]
			pi.address 			  		= params[:address]
			pi.gender 						= params[:gender]
			pi.user_id 			  		= user.id

			user.delete unless pi.save
			return pi
		else
		  return user
		end
	end
end
