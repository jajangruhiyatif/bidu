class Auction < ApplicationRecord
	belongs_to :auction_item
	has_many  :log_bid
	has_many :trx, class_name: "Transaction"

  has_many :auction_user, :foreign_key  => "auction_id", dependent: :destroy
  has_many :user, through: :auction_user

	validates :start_price, presence: true
	validates :start_date, presence: true
	validates :interval, presence: true

	def self.generate_winner(auction_id)
		ActiveRecord::Base.transaction do
			log_bid = LogBid.where(auction_id: auction_id)
			top_bidder = log_bid.order(price: :asc).last.user_id
			top_price = log_bid.order(price: :asc).last.price

			auction = Auction.find(auction_id)
			auction.winner = top_bidder;
			auction.top_bid = top_price;
			auction.auction_status = 'Finished';
			if auction.save
				# Create Transaction for winner
				trx = Transaction.new
				trx.trx_no = 'TRX-'+auction.id.to_s+top_bidder.to_s+auction.auction_item.user_id.to_s
				trx.auction_id = auction.id
				trx.trx_date = Time.now.strftime("%Y-%m-%d %H:%M:%S")
				trx.due_date = (Time.now+1.days).strftime("%Y-%m-%d %H:%M:%S")
				trx.status   = "01"
				trx.price    = top_price
				trx.unique_number = rand(999)
				trx.user_id = top_bidder
				trx.auctioner_id = auction.auction_item.user_id
				if(trx.save)
					auction_user = AuctionUser.where(auction_id: auction_id).where(user_id: top_bidder).first
					auction_user.status = 'done'
					auction_user.save
					return true
				else
					ActiveRecord::Rollback
					return false
				end
			else
				ActiveRecord::Rollback
				return false
			end
		end
	end

	def self.create_with_item(item)
		ActiveRecord::Base.transaction do
			# Create Item First
			auction_item = AuctionItem.new
			auction_item.name 						= item[:name]
			auction_item.price 						= item[:price]
			auction_item.description 			= item[:description]
			auction_item.min_description 	= item[:min_description]
			auction_item.plus_description = item[:plus_description]
			auction_item.condition 				= item[:condition]
			auction_item.user_id 					= item[:user_id]
			auction_item.category_id 			= item[:category_id]
			auction_item.brand_id 				= item[:brand_id]
			auction_item.brand_type_id 		= item[:brand_type_id]
			auction_item.production_date 	= item[:production_date]
			auction_item.tag 							= item[:tag]
			if auction_item.save
				# Save Attachment
				unless(item[:front_image].nil?)
					attachment = Attachment.new
					attachment.auction_item_id = auction_item.id
					attachment.file_type = "front-side"
					attachment.name = item[:front_image]
					attachment.save
				end
				unless(item[:back_image].nil?)
					attachment = Attachment.new
					attachment.auction_item_id = auction_item.id
					attachment.file_type = "back-side"
					attachment.name = item[:back_image]
					attachment.save
				end
				unless(item[:left_image].nil?)
					attachment = Attachment.new
					attachment.auction_item_id = auction_item.id
					attachment.file_type = "left-side"
					attachment.name = item[:left_image]
					attachment.save
				end
				unless(item[:right_image].nil?)
					attachment = Attachment.new
					attachment.auction_item_id = auction_item.id
					attachment.file_type = "right-side"
					attachment.name = item[:right_image]
					attachment.save
				end
				unless(item[:top_image].nil?)
					attachment = Attachment.new
					attachment.auction_item_id = auction_item.id
					attachment.file_type = "top-side"
					attachment.name = item[:top_image]
					attachment.save
				end
				unless(item[:bot_image].nil?)
					attachment = Attachment.new
					attachment.auction_item_id = auction_item.id
					attachment.file_type = "bot-side"
					attachment.name = item[:bot_image]
					attachment.save
				end

				# Create Auction
				auction_request = Auction.new
				auction_request.auction_item_id = auction_item.id
				auction_request.start_price 	  = item[:start_price]
				auction_request.start_date 		  = item[:start_date]
				auction_request.interval        = item[:interval]
				auction_request.auction_status  = "New"
				if(auction_request.save)
					return auction_request
				else
					ActiveRecord::Rollback
					return auction_request
				end
			else
				ActiveRecord::Rollback
				return auction_item
			end
		end
	end
end
