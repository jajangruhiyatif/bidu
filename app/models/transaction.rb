class Transaction < ApplicationRecord
	belongs_to :user
	belongs_to :auction

	has_attached_file :resi_no_image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :resi_no_image, content_type: /\Aimage\/.*\z/
end
