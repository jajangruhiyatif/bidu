class BrandType < ApplicationRecord
	has_many :auction_item, dependent: :nullify
	belongs_to :brand
end
