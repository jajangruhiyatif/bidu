class User < ApplicationRecord
  has_many :auction_item
  has_many :log_bid
  has_one  :personal_information
  has_many :trx, class_name: "Transaction"
  has_many :notification
  has_many :user_address

  has_many :auction_user, :foreign_key  => "user_id", dependent: :destroy
  has_many :auction, through: :auction_user

  attr_accessor :password

  before_save :encrypt_password

  validates :email, presence: true, uniqueness: true
  validates :phone, presence: true, uniqueness: true

  validates_confirmation_of :password
  validates_presence_of :password, on: :create

  def self.authenticate(params)
  	username = params[:username].downcase.strip
  	password = params[:password].strip

    user = find_by(email: username)
    user = find_by(phone: username) if user.blank?

    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      return nil
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
end
