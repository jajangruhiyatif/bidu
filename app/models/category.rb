class Category < ApplicationRecord
	has_many :brand, dependent: :destroy
	has_many :auction_item, dependent: :nullify
end
