class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy, :send_item, :dispute]
  before_action :set_personal_information, only: [:index, :show, :payment_all, :payment_detail]
  skip_before_action :verify_authenticity_token, only: [:send_item]

  # GET /transactions
  # GET /transactions.json
  def index
    user_id = session[:user_id]
    @user   = User.find(user_id)
    @transactions      = Transaction.where(user_id: user_id)
    @transactions_self  = Transaction.where(auctioner_id: user_id)
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show
    @pa = PersonalInformation.where(user_id: session[:user_id]).first
    render :layout => false
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  # GET /transactions/1/edit
  def edit
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)

    respond_to do |format|
      if @transaction.save
        format.html { redirect_to @transaction, notice: 'Transaction was successfully created.' }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        format.html { redirect_to @transaction, notice: 'Transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transactions_url, notice: 'Transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def payment_all
    user_id = session[:user_id]
    @transactions = Transaction.where(user_id: user_id).where(status: "01")
  end

  def payment_detail
    user_id = session[:user_id]
    @transaction = Transaction.where(trx_no: params[:id]).first
  end

  def payment_create
    user_id = session[:user_id]
    @transaction = Transaction.where(trx_no: params[:id]).first
    @transaction.status = "02"
    @transaction.save
    flash[:success] = 'Terimakasih telah melakukan pembayaran, akan di konfirmasi paling lambat 1x24 jam'
    redirect_to transactions_path
  end

  def auctioner_confirmation
    user_id = session[:user_id]
    @transaction = Transaction.where(trx_no: params[:id]).first
    @transaction.status = "04"
    @transaction.save
    flash[:success] = 'Transaksi telah di konfirmasi, segera lakukan pengiriman barang'
    redirect_to transactions_path
  end

  def send_item
    @transaction.resi_no = params[:resi_no]
    @transaction.resi_no_image = params[:resi_no_image]
    @transaction.status = "05"
    @transaction.save
    flash[:success] = 'Terimakasih telah melakukan pengiriman barang'
    redirect_to transactions_path
  end

  def received
    user_id = session[:user_id]
    @transaction = Transaction.where(trx_no: params[:id]).first
    @transaction.status = "08"
    @transaction.save
    flash[:success] = 'Terimakaih atas konfirmasi anda, ikuti lelang lainnya.'
    redirect_to transactions_path
  end

  def dispute
    @transaction.status = "07"
    @transaction.save
    flash[:success] = 'Pegajuan pengemabalian barang anda akan segera di proses, terimakash.'
    redirect_to transactions_path
  end

  def dispute_approve
    @transaction.status = "07.a"
    @transaction.save
    flash[:success] = 'Pengembalian barang telah diterima, barang akan segera di kembalikan oleh pemenang'
    redirect_to transactions_path
  end

  def dispute_denied
    @transaction.status = "07.b"
    @transaction.save
    flash[:success] = 'Pengembalian barang ditolak, peganjuan ini akan segera di verifikasi oleh pihak kami'
    redirect_to transactions_path
  end

  private
  def set_transaction
    @transaction = Transaction.find(params[:id])
  end

  def set_personal_information
    user_id = session[:user_id]
    @pa = PersonalInformation.where(user_id: user_id).first
  end

  def transaction_params
    params.require(:transaction).permit(:trx_no, :auction_id, :trx_date, :due_date, :status, :price, :unique_number, :user_id, :resi_no, :address_id)
  end
end
