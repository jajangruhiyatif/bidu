class SettingsController < ApplicationController
  before_action :set_city, only: [:index]

	def index
		user_id = session[:user_id]
    @user = User.find(user_id)
    @categories = Category.all
    @pa = PersonalInformation.where(user_id: user_id).first
    @personal_information = @user.personal_information
	end

	def update_profile
    user_id = session[:user_id]
    @user   = User.find(user_id)
    @personal_information = @user.personal_information

    if @personal_information.update(personal_information_params)
      flash[:success] = 'Biodata telah diperbaharui'
      redirect_to settings_path
    else
      redirect_to settings_path,  notice: @personal_information.errors.messages
    end
  end

  def create_address
    user_id = session[:user_id]
    @user   = User.find(user_id)
    @address = @user.user_address.new(address_params)

    if @address.save
      flash[:success] = 'Alamat telah di tambahakan'
      redirect_to settings_path
    else
      redirect_to settings_path,  notice: @personal_information.errors.messages
    end
  end

  private
  def personal_information_params
    params.require(:personal_information).permit(:first_name, :last_name, :gender, :birth_place, :birth_date, :address, :phone_number, :telephone, :profile_picture, :username, :password, :email, :user_id, :nik, :ktp)
  end

  def address_params
    params.permit(:name, :city_id, :province_id, :province, :city_type, :city_name, :postal_code, :description, :receiver)
  end

  def set_city
  	# @cities = {}
    conn = Faraday.new
    res = conn.get do |req|
      req.url "https://api.rajaongkir.com/starter/city"
      req.params['key'] = "2c8fa315ba9e1bf12dd2d10a05298a55"
    end
    data = JSON.load(res.body)
    if(data.present?)
      @cities = JSON.parse(data['rajaongkir']['results'].to_json, object_class: OpenStruct)
    else
      @cities = {}
    end
  end
end
