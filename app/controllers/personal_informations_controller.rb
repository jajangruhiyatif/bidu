class PersonalInformationsController < ApplicationController
  before_action :set_personal_information, only: [:show, :edit, :update, :destroy]

  # GET /personal_informations
  # GET /personal_informations.json
  def index
    @personal_informations = PersonalInformation.all
  end

  # GET /personal_informations/1
  # GET /personal_informations/1.json
  def show
  end

  # GET /personal_informations/new
  def new
    @personal_information = PersonalInformation.new
  end

  # GET /personal_informations/1/edit
  def edit
  end

  def get_by_user
    @user = User.find(params[:user_id])
    @personal_information = @user.personal_information
    respond_to do |format|
      if @personal_information
        format.json { render :show, status: :ok, location: @personal_information }
      else
        format.json { render json: @personal_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /personal_informations
  # POST /personal_informations.json
  def create
    @personal_information = PersonalInformation.create_personal_information(personal_information_params)

    respond_to do |format|
      if @personal_information.errors.messages.blank?
        flash[:success] = 'User was successfully created.'
        format.html { redirect_to personal_informations_path }
        format.json { render :show, status: :created, location: @personal_information }
      else
        format.html { redirect_to new_personal_information_path,  notice: @personal_information.errors.messages }
        format.json { render json: @personal_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /personal_informations/1
  # PATCH/PUT /personal_informations/1.json
  def update
    respond_to do |format|
      if @personal_information.update(personal_information_params)
        flash[:success] = 'User was successfully updated.'
        format.html { redirect_to personal_informations_path }
        format.json { render :show, status: :ok, location: @personal_information }
      else
        format.html { redirect_to new_personal_information_path,  notice: @personal_information.errors.messages }
        format.json { render json: @personal_information.errors, status: :unprocessable_entity }
      end
    end
  end

  def verified
    pi = PersonalInformation.find(params[:id])
    pi.verification_status = true
    if pi.save
      flash[:success] = 'User was successfully Verified.'
    else
      flash[:danger] = 'User was failed Verified.'
    end
    redirect_to personal_informations_path
  end

  # DELETE /personal_informations/1
  # DELETE /personal_informations/1.json
  def destroy
    @personal_information.destroy
    respond_to do |format|
      flash[:success] = 'User was successfully destroyed.'
      format.html { redirect_to personal_informations_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_personal_information
      @personal_information = PersonalInformation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def personal_information_params
      params.require(:personal_information).permit(:first_name, :last_name, :gender, :birth_place, :birth_date, :address, :phone_number, :telephone, :profile_picture, :username, :password, :email, :user_id)
    end
end
