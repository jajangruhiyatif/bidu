class HomesController < ApplicationController
  before_action :require_login, only: [:dashboard, :auction_detail, :schedule]
	def index
  	if (session[:role]=='admin')
      redirect_to be_dashboard_index_path
    else
    	@auctions = Auction.where(auction_status: 'Approved').order(created_at: :desc)
      @auctions_popular = Auction.where(auction_status: 'Approved').order(price_bid: :desc)
    end
  end

  def payment_confirmation
    user_id = session[:user_id]
    @transactions = Transaction.where(user_id: user_id).where(status: "01")
    @pa = PersonalInformation.where(user_id: user_id).first
  end

  def payment_detail
    user_id = session[:user_id]
    @transaction = Transaction.where(trx_no: params[:id]).first
    @pa = PersonalInformation.where(user_id: user_id).first
  end

  def payment_process
  end

  def auction_detail
    @auction = Auction.find(params[:id])
  end

  def schedule
  end

  def auctions
  end
end
