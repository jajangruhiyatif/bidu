class AuctionsController < ApplicationController
  before_action :set_auction, only: [:show]
  before_action :require_login, only: [:bid]
  skip_before_action :verify_authenticity_token, only: [:bid]

  def show
  end

  def bid
    @auction = Auction.find(params[:auction_id])
    if @auction.log_bid.order(price: :asc).last
      bid_raise = @auction.log_bid.order(price: :asc).last.price.to_i+@auction.price_bid.to_i
    else
      bid_raise = @auction.start_price+@auction.price_bid.to_i
    end

    @log_bid = LogBid.create_bid(@auction.id, params[:bidder_id], bid_raise)
    respond_to do |format|
      if @log_bid.errors.messages.blank?
        format.json { render :bid, status: :ok, location: @auction }
      else
        format.json { render json: @log_bid.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def set_auction
    @auction = Auction.find(params[:id])
  end
end
