class Api::BrandsController < ApplicationController
	before_action :set_brand, only: [:show]

  def index
    @brands = Brand.all
    if @brands
    	render json:{
    		status: 200,
    		messages: "Get Brands",
    		data: @brands
    	}
    else
    	render json:{
    		status: 404,
    		messages: "Brands not found",
    		data: @brands
    	}
    end
  end

  def show
  	if @brand
    	render json:{
    		status: 200,
    		messages: "Get brand",
    		data: @brand
    	}
    else
    	render json:{
    		status: 404,
    		messages: "brand not found",
    		data: @brand
    	}
    end
  end

  private
    def set_brand
      @brand = Brand.where(id: params[:id]).first
    end
end
