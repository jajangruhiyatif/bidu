class Api::AuctionsController < ApplicationController
	before_action :set_auction, only: [:show, :log, :is_top_bidder]
  before_action :set_user, only: [:is_top_bidder]
  skip_before_action :verify_authenticity_token, only: [:bid, :create]

  def index
    @auctions = Auction.where(auction_status: "Approved")

    respond_to do |format|
      if @auctions
        format.json { render :index, status: 200, message: "Get Auctions" }
      else
      	format.json { render :index, status: 400, message: "Auctions no found" }
      end
    end
  end

  def by_type
		type  = params[:type] ? params[:type] : 'all'
		limit = params[:limit] ? params[:limit] : 5
		case type
		when 'new'
			@auctions = Auction.order(created_at: :asc).limit(limit)
		when 'popular'
      @auctions = Auction.order(created_at: :asc).limit(limit)
    else
			@auctions = Auction.limit(limit)
		end
    respond_to do |format|
      if @auctions
        format.json { render :index, status: 200, message: "Get Auctions" }
      else
      	format.json { render :index, status: 400, message: "Auctions no found" }
      end
    end
  end

  def show
  	respond_to do |format|
      if @auction
        format.json { render :show, status: 200, message: "Get Auctions" }
      else
      	format.json { render :show, status: 400, message: "Auctions no found" }
      end
    end
  end

  def log
  	if @auction
    	render json: {
    		status: 200,
    		message: "Get Auction Log",
    		data: @auction.log_bid
    	}
    else
    	render json: {
    		status: 404,
    		message: "Auction Log not found",
    		data: {}
    	}
    end
  end

  def create
    @auction = Auction.create_with_item(auction_params)

    respond_to do |format|
      if @auction.errors.messages.blank?
        format.json { render :show, status: :created }
      else
        format.json { render json: @auction.errors, status: :unprocessable_entity }
      end
    end
  end

  def bid
    @auction = Auction.find(params[:auction_id])
    if @auction.log_bid.order(price: :asc).last
      bid_raise = @auction.log_bid.order(price: :asc).last.price.to_i+@auction.price_bid.to_i
    else
      bid_raise = @auction.start_price+@auction.price_bid.to_i
    end

    @log_bid = LogBid.create_bid(@auction.id, params[:bidder_id], bid_raise)
    respond_to do |format|
      if @log_bid.errors.messages.blank?
        format.json { render :bid, status: :ok, location: @auction }
      else
        format.json { render json: @log_bid.errors, status: :unprocessable_entity }
      end
    end
  end

  def is_top_bidder
    if @auction
      top_bidder = @auction.log_bid.order(price: :desc).first.user_id
      if @user
        if top_bidder==@user.id
          render json: {
            status: "200",
            message: "user has a top bidder",
            result: true
          }
        else
          render json: {
            status: "404",
            message: "user not a top bidder",
            result: false
          }
        end
      else
        render json: { status: "404", message: "user not found", result: false}
      end
    else
      render json: { status: "404", message: "auction not found", result: false}
    end
  end

  private
  def set_auction
    @auction = Auction.where(id: params[:id]).first
  end

  def set_user
    @user = User.where(id: params[:user_id]).first
  end

  def auction_params
    params.permit(:name, :price, :description, :min_description, :plus_description, :condition, :user_id, :category_id, :brand_id, :brand_type_id, :production_date, :tag, :front_image, :back_image, :left_image, :right_image, :top_image, :bot_image, :start_price, :start_date, :interval)
  end
end
