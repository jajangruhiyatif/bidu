class Api::ServiceController < ApplicationController
	skip_before_action :verify_authenticity_token
	before_action :prepare_rajaongkir, only: [:province, :city, :generate_cost]

	def province
		conn = Faraday.new
		res = conn.get do |req|
		  req.url 'https://api.rajaongkir.com/starter/province'
		  req.params['key'] = @key
		end
		data = JSON.load(res.body)
		if(data.present?)
			render json: { status: 200, message: "Fetch data province", data: data['rajaongkir']['results'] }
		else
			render json: { status: 404, message: "province data not available" }
		end
	end

	def city
		conn = Faraday.new
		res = conn.get do |req|
		  req.url 'https://api.rajaongkir.com/starter/city'
		  req.params['key'] = @key
		  req.params['province'] = @province
		  req.params['id'] 			 = @city
		end
		data = JSON.load(res.body)
		if(data.present?)
			render json: { status: 200, message: "Fetch data city", data: data['rajaongkir']['results'] }
		else
			render json: { status: 404, message: "city data not available" }
		end
	end

	def generate_cost
		conn  = Faraday.new
		temp  = cost_estimate_params
		res   = conn.post do |req|
			req.url 'https://api.rajaongkir.com/starter/cost'
			req.headers['Content-Type'] = 'application/json'
			req.body = '{
				"key" : "'+@key.to_s+'",
				"origin" : "'+temp[:origin]+'",
				"destination" : "'+temp[:destination]+'",
				"weight" : '+temp[:weight].to_s+',
				"courier" : "'+temp[:courier]+'"
			}'
	 	end
	 	data = JSON.load(res.body)
		if(data.present?)
			render json: { status: 200, message: "generate delivery costs", data: data['rajaongkir']['results'] }
		else
			render json: { status: 404, message: "city data not available" }
		end
	end

	def generate_payment_token
		trx_id    = params[:trx_id]
		total_pay = params[:total_pay]
		response = Veritrans.create_widget_token(
      transaction_details: {
        order_id: trx_id,
        gross_amount: total_pay
      }
    )
    render json: { status: 200, message: "generate payment token", data: { token: response.token} }
	end

	def payment_transaction_update
		@transaction = Transaction.where(id: params[:transaction_id]).first
    @transaction.status 				 = params[:code]
    @transaction.courier_type    = params[:courier_type]
    @transaction.courier_service = params[:courier_service]
    @transaction.courier_etd     = params[:courier_etd]
    @transaction.courier_cost    = params[:courier_cost]
    @transaction.total 					 = params[:courier_price].to_i+@transaction.price.to_i
    if @transaction.save
    	render json: { status: 200, message: "Update payment successfully" }
    else
    	render json: { status: 400, message: "Update payment failed" }
    end
	end

	private
	def prepare_rajaongkir
		@key = '2c8fa315ba9e1bf12dd2d10a05298a55'
		@province 		 = params[:province_id]
		@city     		 = params[:id]
		@origin   		 = params[:origin]
		@destination   = params[:destination]
		@courier   		 = params[:courier]
	end

	def cost_estimate_params
		params.permit(:origin, :destination, :weight, :courier)
	end

	def payment_params
		params.permit(:origin, :destination, :weight, :courier)
	end
end
