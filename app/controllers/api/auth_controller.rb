class Api::AuthController < ApplicationController
	skip_before_action :verify_authenticity_token, only: [:login, :register]

	def login
    user = User.authenticate(auth_params)
    respond_to do |format|
      if user
        res = {
          status: '200',
          message: 'Login Successfully',
          data: {
            id: user.id,
            phone: user.phone,
            email: user.email,
            username: user.username,
            first_name: user.personal_information.first_name,
            last_name: user.personal_information.last_name,
            verification_status: user.personal_information.verification_status
          }
        }
        format.json { render json: res, status: :ok }
        else
        res = {
          status: '404',
          message: 'Username or Password Wrong',
          data: {}
        }
        format.json { render json: res, status: :ok }
      end
    end
  end

  def register
    ActiveRecord::Base.transaction do
      user = User.new(register_params)
      if user.save
        pi = PersonalInformation.new
        pi.user_id = user.id
        pi.first_name   = params[:first_name]
        pi.last_name    = params[:last_name]
        pi.phone_number = params[:phone]
        if pi.save
          status  = "200"
          message = "Registration successfully"
          data    = user
        else
          ActiveRecord::Rollback
          status  = 400
          message = pi.errors
          data    = {}
        end
      else
        ActiveRecord::Rollback
        status  = 400
        message = user.errors
        data    = {}
      end
      respond_to do |format|
        format.json { render json: {status: status, message: message, data: data}, status: status }
      end
    end
  end

  private
  def auth_params
    params.permit(:username, :password)
  end

  def register_params
  	params.permit(:email, :phone, :password, :password_confirmation)
  end
end
