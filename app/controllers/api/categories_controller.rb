class Api::CategoriesController < ApplicationController
	before_action :set_category, only: [:show]

  def index
    @categories = Category.all
    if @categories
    	render json: {
    		status: 200,
    		message: "Get Categories",
    		data: @categories
    	}
    else
    	render json: {
    		status: 404,
    		message: "Categories not found",
    		data: {}
    	}
    end
  end

  def show
  	if @category
    	render json: {
    		status: 200,
    		message: "Get Category",
    		data: @category
    	}
    else
    	render json: {
    		status: 404,
    		message: "Category not found",
    		data: {}
    	}
    end
  end

  private
  def set_category
    @category = Category.where(id: params[:id]).first
  end
end
