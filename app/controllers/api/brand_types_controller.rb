class Api::BrandTypesController < ApplicationController
	before_action :set_brand_type, only: [:show]

  def index
    @brand_types  = BrandType.all
    if @brand_types
    	render json:{
    		status: 200,
    		messages: "Get Brand Types",
    		data: @brand_types
    	}
    else
    	render json:{
    		status: 404,
    		messages: "Brand Types not found",
    		data: @brand_types
    	}
    end
  end

  def show
  	if @brand_type
    	render json:{
    		status: 200,
    		messages: "Get Brand Type",
    		data: @brand_type
    	}
    else
    	render json:{
    		status: 404,
    		messages: "Brand Type not found",
    		data: @brand_type
    	}
    end
  end

  private
  def set_brand_type
    @brand_type = BrandType.where(id: params[:id]).first
  end
end
