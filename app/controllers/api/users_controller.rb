class Api::UsersController < ApplicationController
	before_action :set_user, only: [:personal_information, :update_profile]
  skip_before_action :verify_authenticity_token, only: [:update_profile]

	def personal_information
    @personal_information = @user.personal_information
    respond_to do |format|
      if @personal_information
        format.json { render :personal_information, status: :ok }
      else
        format.json { render json: @personal_information.errors, status: :unprocessable_entity }
      end
    end
	end

  def update_profile
    @personal_information = @user.personal_information
    respond_to do |format|
      if @personal_information.update(personal_information_params)
        format.json { render :personal_information, status: :ok }
      else
        format.json { render json: @personal_information.errors, status: :unprocessable_entity }
      end
    end
  end

	private
	def set_user
		@user = User.where(id: params[:user_id]).first
	end

  def personal_information_params
    params.permit(:first_name, :last_name, :gender, :birth_place, :birth_date, :address, :phone_number, :telephone, :profile_picture, :username, :password, :email, :user_id, :nik, :ktp)
  end
end
