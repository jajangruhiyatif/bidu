class Api::TransactionsController < ApplicationController
	before_action :set_transaction, only: [:show]
	def index
		user_id = params[:user_id]
		@transactions = Transaction.where(user_id: user_id)

		respond_to do |format|
      if @transactions
        format.json { render :index, status: 200, message: "Get Transaction" }
      else
      	format.json { render :index, status: 400, message: "Transaction no found" }
      end
    end
	end

	def selling_transaction
		user_id = params[:user_id]
		@transactions = Transaction.where(auctioner_id: user_id)

		respond_to do |format|
      if @transactions
        format.json { render :index, status: 200, message: "Get Transaction" }
      else
      	format.json { render :index, status: 400, message: "Transaction no found" }
      end
    end
	end

	def show
		respond_to do |format|
      if @transaction
        format.json { render :show, status: 200, message: "Get Transaction" }
      else
      	format.json { render :show, status: 400, message: "Transaction no found" }
      end
    end
	end

	private
  def set_transaction
    @transaction = Transaction.where(id: params[:id]).first
  end
end
