class SessionsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:app_signin]

  def new; end
  def signin; end

  def register
  	user = User.new(register_params)
    if user.save
      pi = PersonalInformation.new
      pi.user_id = user.id
      pi.first_name   = params[:first_name]
      pi.last_name    = params[:last_name]
      pi.phone_number = params[:phone]
      if pi.save
        flash[:success] = "Register success"
        redirect_to login_path
      else
        flash[:success] = pi.errors.messages
        redirect_to signin_path
      end
    else
      flash[:danger] = user.errors.messages
      redirect_to signin_path
    end
  end

  def create
    user = User.authenticate(auth_params)
    if user
      session[:user_id] = user.id
      session[:role] = user.is_admin ? 'admin' : 'default'
      session[:fullname] = user.is_admin ? 'Administrator' : user.personal_information.first_name+" "+user.personal_information.last_name
      session[:avatar] = user.is_admin ? '/img/default/user-default.png' : ((user.personal_information.profile_picture.present?) ? user.personal_information.profile_picture(:thumb) : '/img/default/user-default.png')
      flash[:success] = "Logged in!"
      if (session[:role]=='admin')
        redirect_to root_path
      else
        redirect_to dashboards_path
      end
    else
      flash[:danger] = "Invalid email or password"
      redirect_to login_path
    end
  end

  def app_signin
    user = User.authenticate(auth_params)
    respond_to do |format|
      if user
        res = {
          status: '200',
          message: 'Login Successfully',
          data: {
            id: user.id,
            phone: user.phone,
            email: user.email,
            username: user.username,
            first_name: user.personal_information.first_name,
            last_name: user.personal_information.last_name,
            verification_status: user.personal_information.verification_status
          }
        }
        format.json { render json: res, status: :ok }
        else
        res = {
          status: '404',
          message: 'Username or Password Wrong',
          data: {}
        }
        format.json { render json: res, status: :ok }
      end
    end
  end

  def destroy
    session[:user_id] = nil
    session[:role] = nil
    flash[:success] = "Logged out!"
    redirect_to root_path
  end

  private
  def auth_params
    params.permit(:username, :password)
  end

  def register_params
  	params.permit(:email, :phone, :password, :password_confirmation)
  end
end
