class ApplicationController < ActionController::Base
  helper_method :current_user

  def require_login
    if params[:token]=='janganlupa123'
      return true
    else
      if current_user.blank?
        redirect_to login_path, notice: "Please login"
      end
    end
  end

  def require_admin_login
    if current_user.blank?
      redirect_to login_path, notice: "Please login"
    else
      if session[:role]!='admin'
        redirect_to login_path, notice: "Permission denied !"
      end
    end
  end

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id].present?
  end
end
