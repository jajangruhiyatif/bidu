class Backend::AuctionsController < ApplicationController
	before_action :set_auction, only: [:show, :edit, :update, :destroy]
  before_action :require_login, only: [:bid]
  skip_before_action :verify_authenticity_token, only: [:bid, :create_with_item]

  def index
    @auctions = Auction.all
  end

  def show
  end

  def new
    @auction = Auction.new
    @item_used = Auction.all.select(:auction_item_id)
    @auction_items = AuctionItem.where.not(id: @item_used)
  end

  def edit
    @auction_items = AuctionItem.all
  end

  def create
    @auction = Auction.new(auction_params)

    if @auction.save
      flash[:success] = 'Auction was successfully created.'
      redirect_to be_auctions_path
    else
      redirect_to new_be_auction_path, notice: @auction.errors.messages
    end
  end

  def update
    if @auction.update(auction_params)
      flash[:success] = 'Auction was successfully updated.'
      redirect_to be_auctions_path
    else
      redirect_to new_be_auction_path, notice: @auction.errors.messages
    end
  end

  def destroy
    @auction.destroy
    flash[:success] = 'Auction was successfully destroyed.'
    redirect_to be_auctions_path
  end

  def approve
    auction = Auction.find(params[:auction_id])
    interval =  auction.interval.to_i
    auction.end_date = auction.start_date + interval.day
    auction.price_bid = 10000
    auction.auction_status = "Approved"
    auction.auction_type = "general"
    auction.join_fee = 0
    if auction.save
      Auction.delay(run_at: auction.end_date).generate_winner(auction.id)
      flash[:success] = 'Auction was successfully approved.'
      auction_item = AuctionItem.find(auction.auction_item_id)
      auction_item.is_approve = true
      auction_item.save
    else
      flash[:danger] = 'Auction was failed approved.'
    end

    redirect_to be_auctions_path
  end

  def ready
    @auctions = Auction.where("auction_status = 'Approved'")
  end

  def running
    @auctions = Auction.where("auction_status = 'Running'")
  end

  def finished
    @auctions = Auction.where("auction_status = 'Finished'")
  end

  private
  def set_auction
    @auction = Auction.find(params[:id])
  end

  def auction_params
    params.require(:auction).permit(:auction_item_id, :start_price, :start_date, :interval)
  end
end
