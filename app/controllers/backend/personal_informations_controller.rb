class Backend::PersonalInformationsController < ApplicationController
	before_action :set_personal_information, only: [:show, :edit, :update, :destroy]

  def index
    @personal_informations = PersonalInformation.all
  end

  def show
  end

  def new
    @personal_information = PersonalInformation.new
  end

  def edit
  end

  def create
    @personal_information = PersonalInformation.create_personal_information(personal_information_params)

    if @personal_information.errors.messages.blank?
      flash[:success] = 'User was successfully created.'
      redirect_to be_personal_informations_path
    else
    	redirect_to new_be_personal_information_path,  notice: @personal_information.errors.messages
    end
  end

  def update
    if @personal_information.update(personal_information_params)
      flash[:success] = 'User was successfully updated.'
      redirect_to be_personal_informations_path
    else
    	redirect_to new_be_personal_information_path,  notice: @personal_information.errors.messages
    end
  end

  def verified
    pi = PersonalInformation.find(params[:id])
    pi.verification_status = true
    if pi.save
      flash[:success] = 'User was successfully Verified.'
    else
      flash[:danger] = 'User was failed Verified.'
    end
    redirect_to be_personal_informations_path
  end

  def destroy
    @personal_information.destroy
    flash[:success] = 'User was successfully destroyed.'
    redirect_to be_personal_informations_path
  end

  private
  def set_personal_information
    @personal_information = PersonalInformation.find(params[:id])
  end

  def personal_information_params
    params.permit(:first_name, :last_name, :gender, :birth_place, :birth_date, :address, :phone_number, :telephone, :profile_picture, :username, :password, :email, :user_id)
  end
end
