class Backend::DashboardController < ApplicationController
	def index
		@pi = PersonalInformation.all
		@auction = Auction.all.order(created_at: :desc)
		@auction_running = Auction.where("auction_status = 'Approved'").order(created_at: :desc)
		@pi_verified = PersonalInformation.where(verification_status: true).count
		@pi_unverified = PersonalInformation.where(verification_status: false).count
		@auction_request = Auction.where(auction_status: 'New').count
		@auction_ongoing = Auction.where(auction_status: 'Approved').count
		@auction_finished = Auction.where(auction_status: 'Finishied').count
	end
end
