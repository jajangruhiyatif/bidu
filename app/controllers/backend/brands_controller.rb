class Backend::BrandsController < ApplicationController
	before_action :set_brand, only: [:show, :edit, :update, :destroy]

  def index
    @brands 		= Brand.all
    @categories = Category.all
    @brand 			= Brand.new
  end

  def show
  end

  def new
    @brands 		= Brand.all
    @categories = Category.all
    @brand 			= Brand.new
    render 'index'
  end

  def edit
    @brands 		= Brand.all
    @categories = Category.all
    render 'index'
  end

  def create
    @brand = Brand.new(brand_params)

    if @brand.save
      redirect_to be_brands_path, notice:{
      	:success => 'Brand was successfully created.'
      }
    else
      redirect_to be_brands_path, notice: {
      	:danger => @brand.errors.messages
      }
    end
  end

  def update
    if @brand.update(brand_params)
      redirect_to be_brands_path, notice:{
      	:success => 'Brand was successfully updated.'
      }
    else
      redirect_to be_brands_path, notice: {
      	:danger => @brand.errors.messages
      }
    end
  end

  def destroy
    @brand.destroy
    redirect_to be_brands_path, notice:{
    	:success => 'Brand was successfully destroyed.'
    }
  end

  private
  def set_brand
    @brand = Brand.find(params[:id])
  end

  def brand_params
    params.require(:brand).permit(:name, :tags, :category_id)
  end
end
