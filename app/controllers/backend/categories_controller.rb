class Backend::CategoriesController < ApplicationController
  before_action :require_admin_login
	before_action :set_category, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.all
    @category   = Category.new
  end

  def show
  end

  def new
    @categories = Category.all
    @category   = Category.new
    render 'index'
  end

  def edit
    @categories = Category.all
    render 'index'
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      redirect_to be_categories_path, notice: {
        :success => 'Category was successfully created.'
      }
    else
      redirect_to be_categories_path, notice: {
        :danger => @category.errors.messages
      }
    end
  end

  def update
    if @category.update(category_params)
        redirect_to be_categories_path, notice: {
          :success => 'Category was successfully updated.'
        }
    else
      redirect_to be_categories_path, notice: {
        :danger => @category.errors.messages
      }
    end
  end

  def destroy
    @category.destroy
    redirect_to be_categories_path, notice: {
      :success => 'Category was successfully destroyed.'
    }
  end

  private
  def set_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name, :tags)
  end
end
