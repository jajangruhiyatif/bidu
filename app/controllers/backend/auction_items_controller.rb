class Backend::AuctionItemsController < ApplicationController
	before_action :require_login
  before_action :set_auction_item, only: [:show, :edit, :update, :destroy]

  def index
    @auction_items = AuctionItem.all
  end

  def show
  end

  def new
    @auction_item = AuctionItem.new
    @personal_informations = PersonalInformation.all
    @categories  = Category.all
    @brands 		 = Brand.all
    @brand_types = BrandType.all
  end

  def edit
    @personal_informations = PersonalInformation.all
    @categories  = Category.all
    @brands      = Brand.all
    @brand_types = BrandType.all
  end

  def create
    @auction_item = AuctionItem.create_auction_item(auction_item_params)

    if @auction_item.errors.messages.blank?
      flash[:success] = 'Auction was successfully created.'
      redirect_to be_auction_items_path
    else
      redirect_to new_be_auction_item_path, notice: @auction_item.errors.messages
    end
  end

  def update
    if @auction_item.update(auction_item_params)
      flash[:success] = 'Auction was successfully updated.'
      redirect_to be_auction_items_path
    else
      redirect_to new_be_auction_item_path, notice: @auction_item.errors.messages
    end
  end

  def destroy
    @auction_item.destroy
    flash[:success] = 'Auction was successfully destroyed.'
    redirect_to be_auction_items_path
  end

  def pictures
    @auction_item = AuctionItem.find(params[:auction_item_id])
    @pictures     = Attachment.where(auction_item_id:@auction_item.id)
    @attachment   = @auction_item.attachment.new
  end

  def create_picture
    @auction_item = AuctionItem.find(params[:auction_item_id])
    @attachment = @auction_item.attachment.new(attachment_params)
    if @attachment.save
      redirect_to be_auction_item_pictures_path(@auction_item), notice:{
      	:success => 'Picture was successfully added.'
      }
    else
      redirect_to be_auction_item_pictures_path(@auction_item), notice: @attachment.errors.messages
    end
  end

  def delete_picture
    @auction_item = AuctionItem.find(params[:auction_item_id])
    @attachment = Attachment.find(params[:id])
    @attachment.destroy
    redirect_to be_auction_item_pictures_path, notice: {
    	:success => 'Picture was successfully removed.'
    }
  end

  private
  def set_auction_item
    @auction_item = AuctionItem.find(params[:id])
  end

  def auction_item_params
    params.require(:auction_item).permit(:name, :price, :description, :min_description, :plus_description, :condition, :user_id, :category_id, :brand_id, :brand_type_id, :production_date, :tag, :weight, :volume)
  end

  def attachment_params
    params.require(:attachment).permit(:name, :file_type)
  end
end
