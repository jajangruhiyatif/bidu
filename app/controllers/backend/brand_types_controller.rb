class Backend::BrandTypesController < ApplicationController
	before_action :set_brand_type, only: [:show, :edit, :update, :destroy]

  def index
    @brand_types = BrandType.all
    @brands 		 = Brand.all
    @brand_type  = BrandType.new
  end

  def show
  end

  def new
    @brand_types = BrandType.all
    @brands 	   = Brand.all
    @brand_type  = BrandType.new
    render 'index'
  end

  def edit
    @brand_types = BrandType.all
    @brands 		 = Brand.all
    render 'index'
  end

  def create
    @brand_type = BrandType.new(brand_type_params)

    if @brand_type.save
      redirect_to be_brand_types_path, notice: {
      	:success => 'Brand type was successfully created.'
      }
    else
      redirect_to be_brand_types_path, notice: {
      	:danger => @brand_type.errors.messages
      }
    end
  end

  def update
    if @brand_type.update(brand_type_params)
      redirect_to be_brand_types_path, notice: {
      	:success => 'Brand type was successfully updated.'
      }
    else
      redirect_to be_brand_types_path, notice: {
      	:danger => @brand_type.errors.messages
      }
    end
  end

  def destroy
    @brand_type.destroy
    redirect_to be_brand_types_path, notice: {
    	:success => 'Brand type was successfully destroyed.'
    }
  end

  private
  def set_brand_type
    @brand_type = BrandType.find(params[:id])
  end

 	def brand_type_params
    params.require(:brand_type).permit(:name, :tags, :brand_id)
  end
end
