class Backend::BidPriceGapsController < ApplicationController
	before_action :set_bid_price_gap, only: [:show, :edit, :update, :destroy]

  def index
    @bid_price_gaps = BidPriceGap.all
    @bid_price_gap  = BidPriceGap.new
  end

  def show
  end

  def new
    @bid_price_gaps = BidPriceGap.all
    @bid_price_gap  = BidPriceGap.new
    render 'index'
  end

  def edit
    @bid_price_gaps = BidPriceGap.all
    render 'index'
  end

  def create
    @bid_price_gap = BidPriceGap.new(bid_price_gap_params)

    if @bid_price_gap.save
      redirect_to be_bid_price_gaps_path, notice: {
      	:success => 'Category was successfully created.'
      }
    else
      redirect_to be_bid_price_gaps_path, notice: {
      	:danger => @bid_price_gap.errors.messages
      }
    end
  end

  def update
    if @bid_price_gap.update(bid_price_gap_params)
      redirect_to be_bid_price_gaps_path, notice: {
      	:success => 'Category was successfully created.'
      }
    else
      redirect_to be_bid_price_gaps_path, notice: {
      	:danger => @bid_price_gap.errors.messages
      }
    end
  end

  def destroy
    @bid_price_gap.destroy
    redirect_to be_bid_price_gaps_path, notice: {
    	:success => 'Category was successfully destroyed.'
    }
  end

  private
    def set_bid_price_gap
      @bid_price_gap = BidPriceGap.find(params[:id])
    end

    def bid_price_gap_params
      params.require(:bid_price_gap).permit(:upper_limit, :lower_limit, :price)
    end
end
