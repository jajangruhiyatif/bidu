class Backend::TransactionsController < ApplicationController
	before_action :set_transacation, only: [:show, :payment_approve, :delivery_set]

	def index
		@transactions = Transaction.all
	end

	def payment_confirmation
		@transactions = Transaction.where(status: "02")
	end

	def payment_approve
		@transaction.status = "03"
		if @transaction.save
			flash[:success] = 'Payment has approved successfully.'
      redirect_to be_transactions_path
    else
    	redirect_to be_transactions_path,  notice: @transaction.errors.messages
    end
	end

	def delivery_set
		@transaction.status = "06"
		if @transaction.save
			flash[:success] = 'Transaction has set as delivered.'
      redirect_to be_transactions_path
    else
    	redirect_to be_transactions_path,  notice: @transaction.errors.messages
    end
	end

	private

	def set_transacation
    @transaction = Transaction.find(params[:id])
  end

  def approve_param
    params.permit(:first_name, :last_name, :gender, :birth_place, :birth_date, :address, :phone_number, :telephone, :profile_picture, :username, :password, :email, :user_id)
  end
end
