class DashboardsController < ApplicationController
  before_action :require_login

  def index
  	user_id = session[:user_id]
    @auction_user = AuctionUser.where(user_id: user_id)
    @auction_self = AuctionItem.where(user_id: user_id)
    @pa = PersonalInformation.where(user_id: user_id).first
  end
end
