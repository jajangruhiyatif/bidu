json.extract! transaction, :id, :trx_no, :auction_id, :trx_date, :due_date, :status, :price, :unique_number, :user_id, :resi_no, :address_id, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)
