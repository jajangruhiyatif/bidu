json.extract! transaction, :id, :trx_no, :total, :trx_date, :status
json.partial! "api/auctions/auction", auction: transaction.auction