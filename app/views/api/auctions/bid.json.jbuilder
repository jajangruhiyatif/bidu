json.extract! @auction, :id, :start_price

json.data @auction.log_bid.order(price: :desc).limit(10) do |log_bid|
  json.partial! "api/auctions/log_bid", log_bid: log_bid
end