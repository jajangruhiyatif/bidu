json.extract! auction, :id, :auction_item_id, :start_price, :start_date, :end_date, :interval, :winner, :bidder, :top_bid, :price_bid, :auction_status, :auction_type, :created_at, :updated_at
json.join_bidder auction.auction_user.count
json.partial! "api/auctions/auction_item", auction_item: auction.auction_item
json.log_bid auction.log_bid.order(price: :desc).limit(10)
