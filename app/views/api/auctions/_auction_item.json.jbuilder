json.extract! auction_item, :user_id, :name, :price, :production_date, :description, :min_description, :plus_description, :condition, :created_at, :updated_at
json.category auction_item.category.name
json.brand auction_item.brand.name
json.brand_type auction_item.brand_type.name
json.attachment auction_item.attachment, :id, :name
