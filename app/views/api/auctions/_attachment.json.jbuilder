json.extract! attachment, :id, :name, :created_at, :updated_at
json.url auction_url(attachment, format: :json)
