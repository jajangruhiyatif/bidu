json.extract! log_bid, :id, :auction_id, :user_id, :price, :created_at, :updated_at
json.url log_bid_url(log_bid, format: :json)
