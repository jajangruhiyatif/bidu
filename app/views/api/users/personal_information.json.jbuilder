json.extract! @personal_information, :id, :user_id, :first_name, :last_name, :gender, :birth_place, :birth_date, :address, :phone_number, :telephone, :nik, :bank_account_id, :verification_status , :created_at, :updated_at
json.ktp @personal_information.ktp(:medium)
json.ktp_thumb @personal_information.ktp(:thumb)
json.profile_picture @personal_information.profile_picture(:medium)
json.profile_picture_thumb @personal_information.profile_picture(:thumb)
