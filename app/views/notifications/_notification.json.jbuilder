json.extract! notification, :id, :name, :message, :type, :user_id, :is_read, :created_at, :updated_at
json.url notification_url(notification, format: :json)
