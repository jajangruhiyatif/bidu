json.extract! user, :id, :phone, :email, :username
json.url user_url(user, format: :json)
