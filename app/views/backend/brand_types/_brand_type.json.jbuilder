json.extract! brand_type, :id, :name, :tags, :brand_id, :created_at, :updated_at
json.url brand_type_url(brand_type, format: :json)
