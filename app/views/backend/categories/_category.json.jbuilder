json.extract! category, :id, :name, :tags, :created_at, :updated_at
json.url category_url(category, format: :json)
