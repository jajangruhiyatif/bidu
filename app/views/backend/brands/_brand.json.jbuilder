json.extract! brand, :id, :name, :tags, :category_id, :created_at, :updated_at
json.url brand_url(brand, format: :json)
