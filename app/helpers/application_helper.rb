module ApplicationHelper
	include Rack::Utils

	def api_format(data, code = 200, message = "")
    {
        :code => code,
        :status => HTTP_STATUS_CODES[code],
        :message => message,
        :data => data
    }
  end
end
