require "application_system_test_case"

class BrandTypesTest < ApplicationSystemTestCase
  setup do
    @brand_type = brand_types(:one)
  end

  test "visiting the index" do
    visit brand_types_url
    assert_selector "h1", text: "Brand Types"
  end

  test "creating a Brand type" do
    visit brand_types_url
    click_on "New Brand Type"

    fill_in "Brand", with: @brand_type.brand_id
    fill_in "Name", with: @brand_type.name
    fill_in "Tags", with: @brand_type.tags
    click_on "Create Brand type"

    assert_text "Brand type was successfully created"
    click_on "Back"
  end

  test "updating a Brand type" do
    visit brand_types_url
    click_on "Edit", match: :first

    fill_in "Brand", with: @brand_type.brand_id
    fill_in "Name", with: @brand_type.name
    fill_in "Tags", with: @brand_type.tags
    click_on "Update Brand type"

    assert_text "Brand type was successfully updated"
    click_on "Back"
  end

  test "destroying a Brand type" do
    visit brand_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Brand type was successfully destroyed"
  end
end
