require "application_system_test_case"

class LogBidsTest < ApplicationSystemTestCase
  setup do
    @log_bid = log_bids(:one)
  end

  test "visiting the index" do
    visit log_bids_url
    assert_selector "h1", text: "Log Bids"
  end

  test "creating a Log bid" do
    visit log_bids_url
    click_on "New Log Bid"

    fill_in "Id lelang", with: @log_bid.id_lelang
    fill_in "Price", with: @log_bid.price
    fill_in "User", with: @log_bid.user_id
    click_on "Create Log bid"

    assert_text "Log bid was successfully created"
    click_on "Back"
  end

  test "updating a Log bid" do
    visit log_bids_url
    click_on "Edit", match: :first

    fill_in "Id lelang", with: @log_bid.id_lelang
    fill_in "Price", with: @log_bid.price
    fill_in "User", with: @log_bid.user_id
    click_on "Update Log bid"

    assert_text "Log bid was successfully updated"
    click_on "Back"
  end

  test "destroying a Log bid" do
    visit log_bids_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Log bid was successfully destroyed"
  end
end
