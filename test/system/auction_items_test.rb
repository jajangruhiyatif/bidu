require "application_system_test_case"

class AuctionItemsTest < ApplicationSystemTestCase
  setup do
    @auction_item = auction_items(:one)
  end

  test "visiting the index" do
    visit auction_items_url
    assert_selector "h1", text: "Auction Items"
  end

  test "creating a Auction item" do
    visit auction_items_url
    click_on "New Auction Item"

    fill_in "Category", with: @auction_item.category_id
    fill_in "End time", with: @auction_item.end_time
    fill_in "Img path", with: @auction_item.img_path
    check "Is approve" if @auction_item.is_approve
    fill_in "Product name", with: @auction_item.product_name
    fill_in "Product price", with: @auction_item.product_price
    fill_in "Start date", with: @auction_item.start_date
    fill_in "User", with: @auction_item.user_id
    click_on "Create Auction item"

    assert_text "Auction item was successfully created"
    click_on "Back"
  end

  test "updating a Auction item" do
    visit auction_items_url
    click_on "Edit", match: :first

    fill_in "Category", with: @auction_item.category_id
    fill_in "End time", with: @auction_item.end_time
    fill_in "Img path", with: @auction_item.img_path
    check "Is approve" if @auction_item.is_approve
    fill_in "Product name", with: @auction_item.product_name
    fill_in "Product price", with: @auction_item.product_price
    fill_in "Start date", with: @auction_item.start_date
    fill_in "User", with: @auction_item.user_id
    click_on "Update Auction item"

    assert_text "Auction item was successfully updated"
    click_on "Back"
  end

  test "destroying a Auction item" do
    visit auction_items_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Auction item was successfully destroyed"
  end
end
