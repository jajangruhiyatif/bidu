require "application_system_test_case"

class BidPriceGapsTest < ApplicationSystemTestCase
  setup do
    @bid_price_gap = bid_price_gaps(:one)
  end

  test "visiting the index" do
    visit bid_price_gaps_url
    assert_selector "h1", text: "Bid Price Gaps"
  end

  test "creating a Bid price gap" do
    visit bid_price_gaps_url
    click_on "New Bid Price Gap"

    fill_in "Lower limit", with: @bid_price_gap.lower_limit
    fill_in "Price", with: @bid_price_gap.price
    fill_in "Upper limit", with: @bid_price_gap.upper_limit
    click_on "Create Bid price gap"

    assert_text "Bid price gap was successfully created"
    click_on "Back"
  end

  test "updating a Bid price gap" do
    visit bid_price_gaps_url
    click_on "Edit", match: :first

    fill_in "Lower limit", with: @bid_price_gap.lower_limit
    fill_in "Price", with: @bid_price_gap.price
    fill_in "Upper limit", with: @bid_price_gap.upper_limit
    click_on "Update Bid price gap"

    assert_text "Bid price gap was successfully updated"
    click_on "Back"
  end

  test "destroying a Bid price gap" do
    visit bid_price_gaps_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Bid price gap was successfully destroyed"
  end
end
