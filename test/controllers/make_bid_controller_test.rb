require 'test_helper'

class MakeBidControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get make_bid_new_url
    assert_response :success
  end

  test "should get create" do
    get make_bid_create_url
    assert_response :success
  end

  test "should get update" do
    get make_bid_update_url
    assert_response :success
  end

  test "should get edit" do
    get make_bid_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get make_bid_destroy_url
    assert_response :success
  end

  test "should get index" do
    get make_bid_index_url
    assert_response :success
  end

  test "should get show" do
    get make_bid_show_url
    assert_response :success
  end

end
