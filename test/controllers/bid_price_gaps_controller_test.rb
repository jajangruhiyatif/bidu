require 'test_helper'

class BidPriceGapsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bid_price_gap = bid_price_gaps(:one)
  end

  test "should get index" do
    get bid_price_gaps_url
    assert_response :success
  end

  test "should get new" do
    get new_bid_price_gap_url
    assert_response :success
  end

  test "should create bid_price_gap" do
    assert_difference('BidPriceGap.count') do
      post bid_price_gaps_url, params: { bid_price_gap: { lower_limit: @bid_price_gap.lower_limit, price: @bid_price_gap.price, upper_limit: @bid_price_gap.upper_limit } }
    end

    assert_redirected_to bid_price_gap_url(BidPriceGap.last)
  end

  test "should show bid_price_gap" do
    get bid_price_gap_url(@bid_price_gap)
    assert_response :success
  end

  test "should get edit" do
    get edit_bid_price_gap_url(@bid_price_gap)
    assert_response :success
  end

  test "should update bid_price_gap" do
    patch bid_price_gap_url(@bid_price_gap), params: { bid_price_gap: { lower_limit: @bid_price_gap.lower_limit, price: @bid_price_gap.price, upper_limit: @bid_price_gap.upper_limit } }
    assert_redirected_to bid_price_gap_url(@bid_price_gap)
  end

  test "should destroy bid_price_gap" do
    assert_difference('BidPriceGap.count', -1) do
      delete bid_price_gap_url(@bid_price_gap)
    end

    assert_redirected_to bid_price_gaps_url
  end
end
