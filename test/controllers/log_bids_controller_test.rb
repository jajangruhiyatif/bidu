require 'test_helper'

class LogBidsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @log_bid = log_bids(:one)
  end

  test "should get index" do
    get log_bids_url
    assert_response :success
  end

  test "should get new" do
    get new_log_bid_url
    assert_response :success
  end

  test "should create log_bid" do
    assert_difference('LogBid.count') do
      post log_bids_url, params: { log_bid: { id_lelang: @log_bid.id_lelang, price: @log_bid.price, user_id: @log_bid.user_id } }
    end

    assert_redirected_to log_bid_url(LogBid.last)
  end

  test "should show log_bid" do
    get log_bid_url(@log_bid)
    assert_response :success
  end

  test "should get edit" do
    get edit_log_bid_url(@log_bid)
    assert_response :success
  end

  test "should update log_bid" do
    patch log_bid_url(@log_bid), params: { log_bid: { id_lelang: @log_bid.id_lelang, price: @log_bid.price, user_id: @log_bid.user_id } }
    assert_redirected_to log_bid_url(@log_bid)
  end

  test "should destroy log_bid" do
    assert_difference('LogBid.count', -1) do
      delete log_bid_url(@log_bid)
    end

    assert_redirected_to log_bids_url
  end
end
