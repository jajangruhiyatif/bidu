Rails.application.routes.draw do

  root "homes#index"

  get  "logout"     => "sessions#destroy", as: "logout"
  get  "signin"     => "sessions#signin", as: "signin"
  post "app-signin" => "sessions#app_signin", as: "app-signin"
  get  "login"      => "sessions#new", as: "login"
  get  "schedule"   => "homes#schedule", as: "schedule"
  get  "admin"      => "admin#index", as: "admin"

  resources :sessions, path: "auth" do
    collection do
      post :register
    end
  end

  resources :homes
  resources :attachments
  resources :dashboards
  resources :notifications
  resources :log_bids
  resources :settings do
    collection do
      patch :update_profile
      post  :create_address
    end
  end
  resources :auctions do
    collection do
      post "bid" => "auctions#bid", as: "bid"
    end
  end
  resources :transactions do
    collection do
      get   "payment-confirmation"       => "transactions#payment_all", as: "payment-all"
      get   "payment-confirmation/:id"   => "transactions#payment_detail", as: "payment-detail"
      patch "payment-confirmation/:id"   => "transactions#payment_create", as: "payment-create"
      patch "auctioner-confirmation/:id" => "transactions#auctioner_confirmation", as: "auctioner-confirmation"
      post  :send_item
      post  :dispute
      patch "received/:id"        => "transactions#received", as: "received"
      patch "dispute-approve/:id" => "transactions#dispute_approve", as: "dispute-approve"
      patch "dispute-denied/:id"  => "transactions#dispute_denied", as: "dispute-denied"
    end
  end

  namespace :api do
    resources :categories
    resources :brands
    resources :brand_types
    resources :transactions do
      collection do
        get :selling_transaction
      end
    end
    resources :auctions do
      collection do
        get  :by_type
        get  :log
        post :bid
        get  :is_top_bidder
      end
    end
    resources :service do
      collection do
        get  :province
        get  :city
        post :generate_cost
        post :generate_payment_token
        post :payment_transaction_update
      end
    end
    resources :auth do
      collection do
        post :login
        post :register
      end
    end
    resources :users do
      collection do
        get   :personal_information
        patch :update_profile
      end
    end
  end

  namespace :backend, as:"be" do
    resources :dashboard
    resources :categories
    resources :brands
    resources :brand_types
    resources :bid_price_gaps
    resources :personal_informations do
      collection do
        post :verified
      end
    end
    resources :auction_items do
      get    "pictures" => "auction_items#pictures"
      post   "pictures" => "auction_items#create_picture"
      delete "pictures/:id" => "auction_items#delete_picture"
    end
    resources :auctions do
      collection do
        get  :ready
        get  :running
        get  :finished
        post :approve
      end
    end
    resources :transactions do
      collection do
        get   :payment_confirmation
        post  :payment_approve
        patch "delivery-set/:id" => "transactions#delivery_set", as: "delivery-set"
      end
    end
  end
end
