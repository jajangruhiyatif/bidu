# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_28_213833) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attachments", force: :cascade do |t|
    t.string "flag"
    t.text "path"
    t.string "file_type"
    t.integer "auction_item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name_file_name"
    t.string "name_content_type"
    t.integer "name_file_size"
    t.datetime "name_updated_at"
  end

  create_table "auction_items", force: :cascade do |t|
    t.integer "user_id"
    t.string "name"
    t.float "price"
    t.text "description"
    t.text "min_description"
    t.text "plus_description"
    t.string "condition"
    t.integer "category_id"
    t.integer "brand_id"
    t.integer "brand_type_id"
    t.string "production_date"
    t.string "tag"
    t.boolean "is_approve", default: false
    t.boolean "is_sold", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "weight"
    t.string "volume"
    t.index ["name"], name: "index_auction_items_on_name"
    t.index ["tag"], name: "index_auction_items_on_tag"
    t.index ["user_id"], name: "index_auction_items_on_user_id"
  end

  create_table "auction_users", force: :cascade do |t|
    t.integer "user_id"
    t.integer "auction_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.index ["auction_id"], name: "index_auction_users_on_auction_id"
    t.index ["user_id"], name: "index_auction_users_on_user_id"
  end

  create_table "auctions", force: :cascade do |t|
    t.integer "auction_item_id"
    t.float "start_price"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string "interval"
    t.integer "winner"
    t.integer "bidder"
    t.float "top_bid"
    t.float "price_bid"
    t.string "auction_status"
    t.string "auction_type"
    t.integer "join_fee"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["auction_item_id"], name: "index_auctions_on_auction_item_id"
    t.index ["auction_status"], name: "index_auctions_on_auction_status"
    t.index ["start_price"], name: "index_auctions_on_start_price"
  end

  create_table "bid_price_gaps", force: :cascade do |t|
    t.string "upper_limit"
    t.string "lower_limit"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "brand_types", force: :cascade do |t|
    t.string "name"
    t.string "tags"
    t.integer "brand_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["brand_id"], name: "index_brand_types_on_brand_id"
    t.index ["name"], name: "index_brand_types_on_name"
    t.index ["tags"], name: "index_brand_types_on_tags"
  end

  create_table "brands", force: :cascade do |t|
    t.string "name"
    t.string "tags"
    t.integer "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_brands_on_category_id"
    t.index ["name"], name: "index_brands_on_name"
    t.index ["tags"], name: "index_brands_on_tags"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "tags"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_categories_on_name"
    t.index ["tags"], name: "index_categories_on_tags"
  end

  create_table "courier_supports", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "log_bids", force: :cascade do |t|
    t.integer "auction_id"
    t.integer "user_id"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["auction_id"], name: "index_log_bids_on_auction_id"
    t.index ["user_id"], name: "index_log_bids_on_user_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "name"
    t.text "message"
    t.string "type"
    t.integer "user_id"
    t.boolean "is_read"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "personal_informations", force: :cascade do |t|
    t.integer "user_id"
    t.string "first_name"
    t.string "last_name"
    t.string "gender"
    t.string "birth_place"
    t.date "birth_date"
    t.text "address"
    t.string "phone_number"
    t.string "telephone"
    t.string "nik"
    t.string "ktp_image"
    t.string "bank_account_id"
    t.boolean "verification_status", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ktp_file_name"
    t.string "ktp_content_type"
    t.integer "ktp_file_size"
    t.datetime "ktp_updated_at"
    t.string "profile_picture_file_name"
    t.string "profile_picture_content_type"
    t.integer "profile_picture_file_size"
    t.datetime "profile_picture_updated_at"
    t.index ["first_name"], name: "index_personal_informations_on_first_name"
    t.index ["phone_number"], name: "index_personal_informations_on_phone_number"
    t.index ["user_id"], name: "index_personal_informations_on_user_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.string "trx_no"
    t.integer "auction_id"
    t.datetime "trx_date"
    t.datetime "due_date"
    t.string "status"
    t.float "price"
    t.integer "unique_number"
    t.integer "user_id"
    t.string "resi_no"
    t.integer "address_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "auctioner_id"
    t.datetime "delivery_date"
    t.string "courier_type"
    t.string "courier_service"
    t.float "courier_cost"
    t.string "courier_etd"
    t.datetime "payment_date"
    t.float "total"
    t.string "resi_no_image_file_name"
    t.string "resi_no_image_content_type"
    t.integer "resi_no_image_file_size"
    t.datetime "resi_no_image_updated_at"
  end

  create_table "user_addresses", force: :cascade do |t|
    t.integer "user_id"
    t.string "name"
    t.integer "city_id"
    t.integer "province_id"
    t.string "city_name"
    t.string "postal_code"
    t.text "description"
    t.string "receiver"
    t.boolean "is_main"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "city_type"
    t.string "province"
  end

  create_table "users", force: :cascade do |t|
    t.string "phone"
    t.string "email"
    t.string "username"
    t.boolean "is_admin", default: false
    t.boolean "is_active", default: true
    t.string "password_hash"
    t.string "password_salt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email"
    t.index ["phone"], name: "index_users_on_phone"
    t.index ["username"], name: "index_users_on_username"
  end

end
