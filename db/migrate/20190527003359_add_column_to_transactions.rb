class AddColumnToTransactions < ActiveRecord::Migration[5.2]
  def change
  	add_column :transactions, :auctioner_id, :integer
  	add_column :transactions, :delivery_date, :datetime
  end
end
