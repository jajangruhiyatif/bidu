class CreateAuctions < ActiveRecord::Migration[5.2]
  def change
    create_table :auctions do |t|
      t.integer  :auction_item_id
      t.integer  :user_id
      t.float    :start_price
      t.datetime :start_date
      t.datetime :end_date
      t.string   :interval
      t.integer  :winner
      t.integer  :bidder
      t.float    :top_bid
      t.float    :price_bid
      t.string   :auction_status
      t.string   :auction_type
      t.integer  :join_fee

      t.timestamps
    end

    add_index :auctions, :user_id
    add_index :auctions, :auction_item_id
    add_index :auctions, :start_price
    add_index :auctions, :auction_status
  end
end
