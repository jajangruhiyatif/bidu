class CreateUserAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :user_addresses do |t|
      t.integer :user_id
      t.string  :name
      t.integer :city_id
      t.integer :province_id
      t.string  :type
      t.string  :city_name
      t.string  :postal_code
      t.text    :description
      t.string  :receiver
      t.boolean :is_main

      t.timestamps
    end
  end
end
