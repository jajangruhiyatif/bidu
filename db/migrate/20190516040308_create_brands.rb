class CreateBrands < ActiveRecord::Migration[5.2]
  def change
    create_table :brands do |t|
      t.string :name
      t.string :tags
      t.integer :category_id

      t.timestamps
    end

    add_index :brands, :name
    add_index :brands, :tags
    add_index :brands, :category_id
  end
end
