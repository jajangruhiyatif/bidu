class CreateBrandTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :brand_types do |t|
      t.string :name
      t.string :tags
      t.integer :brand_id

      t.timestamps
    end

    add_index :brand_types, :name
    add_index :brand_types, :tags
    add_index :brand_types, :brand_id
  end
end
