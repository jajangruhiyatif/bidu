class RemoveProfilePictureFromPersonalInformation < ActiveRecord::Migration[5.2]
  def change
  	remove_column :personal_informations, :profile_picture
  end
end
