class CreateBidPriceGaps < ActiveRecord::Migration[5.2]
  def change
    create_table :bid_price_gaps do |t|
      t.string :upper_limit
      t.string :lower_limit
      t.float  :price

      t.timestamps
    end
  end
end
