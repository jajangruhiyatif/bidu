class FixAuctionItemsTable < ActiveRecord::Migration[5.2]
  def change
  	add_column :auction_items, :weight, :integer
  	add_column :auction_items, :volume, :string
  end
end
