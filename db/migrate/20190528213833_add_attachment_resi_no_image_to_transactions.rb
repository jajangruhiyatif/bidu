class AddAttachmentResiNoImageToTransactions < ActiveRecord::Migration[5.2]
  def self.up
    change_table :transactions do |t|
      t.attachment :resi_no_image
    end
  end

  def self.down
    remove_attachment :transactions, :resi_no_image
  end
end
