class CreateAttachments < ActiveRecord::Migration[5.2]
  def change
    create_table :attachments do |t|
      t.string :flag
      t.text :path
      t.string :file_type
      t.integer  :auction_item_id

      t.timestamps
    end
  end
end
