class AddAttachmentProfilePictureToPersonalInformations < ActiveRecord::Migration[5.2]
  def self.up
    change_table :personal_informations do |t|
      t.attachment :profile_picture
    end
  end

  def self.down
    remove_attachment :personal_informations, :profile_picture
  end
end
