class CreateAuctionItems < ActiveRecord::Migration[5.2]
  def change
    create_table :auction_items do |t|
      t.integer :user_id
      t.string  :name
      t.float   :price
      t.text    :description
      t.text    :min_description
      t.text    :plus_description
      t.string  :condition
      t.integer :category_id
      t.integer :brand_id
      t.integer :brand_type_id
      t.string  :production_date
      t.string  :tag
      t.boolean :is_approve, default: false
      t.boolean :is_sold, default: false

      t.timestamps
    end
    add_index :auction_items, :user_id
    add_index :auction_items, :name
    add_index :auction_items, :tag
  end
end
