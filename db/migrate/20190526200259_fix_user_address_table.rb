class FixUserAddressTable < ActiveRecord::Migration[5.2]
  def change
  	remove_column :user_addresses, :type
  	add_column :user_addresses, :city_type, :string
  	add_column :user_addresses, :province, :string
  end
end
