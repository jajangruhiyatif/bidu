class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.string   :trx_no
      t.integer  :auction_id
      t.datetime :trx_date
      t.datetime :due_date
      t.string   :status
      t.float    :price
      t.integer  :unique_number
      t.integer  :user_id
      t.string   :resi_no
      t.integer  :address_id

      t.timestamps
    end
  end
end
