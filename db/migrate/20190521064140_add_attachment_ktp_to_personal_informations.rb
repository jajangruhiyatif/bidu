class AddAttachmentKtpToPersonalInformations < ActiveRecord::Migration[5.2]
  def self.up
    change_table :personal_informations do |t|
      t.attachment :ktp
    end
  end

  def self.down
    remove_attachment :personal_informations, :ktp
  end
end
