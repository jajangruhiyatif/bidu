class CreatePersonalInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :personal_informations do |t|
      t.integer :user_id
      t.string  :first_name
      t.string  :last_name
      t.string  :gender
      t.string  :birth_place
      t.date    :birth_date
      t.text    :address
      t.string  :phone_number
      t.string  :telephone
      t.string  :profile_picture
      t.string  :nik
      t.string  :ktp_image
      t.string  :bank_account_id
      t.boolean :verification_status, default: false

      t.timestamps
    end

    add_index :personal_informations, :user_id
    add_index :personal_informations, :first_name
    add_index :personal_informations, :phone_number
  end
end
