class AddStatusFieldToAuctionUsers < ActiveRecord::Migration[5.2]
  def change
  	add_column :auction_users, :status, :string
  end
end
