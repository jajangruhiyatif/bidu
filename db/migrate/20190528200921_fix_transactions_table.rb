class FixTransactionsTable < ActiveRecord::Migration[5.2]
  def change
  	add_column :transactions, :courier_type, :string
  	add_column :transactions, :courier_service, :string
  	add_column :transactions, :courier_cost, :float
  	add_column :transactions, :courier_etd, :string
  	add_column :transactions, :payment_date, :datetime
  	add_column :transactions, :total, :float
  end
end
