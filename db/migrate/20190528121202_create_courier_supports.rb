class CreateCourierSupports < ActiveRecord::Migration[5.2]
  def change
    create_table :courier_supports do |t|
      t.string :name
      t.string :code
      t.text :description

      t.timestamps
    end
  end
end
