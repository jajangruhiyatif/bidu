class CreateLogBids < ActiveRecord::Migration[5.2]
  def change
    create_table :log_bids do |t|
      t.integer :auction_id
      t.integer :user_id
      t.float   :price

      t.timestamps
    end

    add_index :log_bids, :auction_id
    add_index :log_bids, :user_id
  end
end
