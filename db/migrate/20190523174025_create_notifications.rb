class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.string  :name
      t.text    :message
      t.string  :type
      t.integer :user_id
      t.boolean :is_read

      t.timestamps
    end
  end
end
