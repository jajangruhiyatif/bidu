class CreateAuctionUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :auction_users do |t|
      t.integer :user_id
      t.integer :auction_id

      t.timestamps
    end

    add_index :auction_users, :user_id
    add_index :auction_users, :auction_id
  end
end
