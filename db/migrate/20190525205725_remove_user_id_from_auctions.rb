class RemoveUserIdFromAuctions < ActiveRecord::Migration[5.2]
  def change
  	remove_column :auctions, :user_id
  end
end
