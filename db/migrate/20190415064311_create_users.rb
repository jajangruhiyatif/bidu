class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :phone
      t.string :email
      t.string :username
      t.boolean :is_admin, :default => false
      t.boolean :is_active, :default => true
      t.string :password_hash
      t.string :password_salt

      t.timestamps
    end

    add_index :users, :phone
    add_index :users, :email
    add_index :users, :username
  end
end
