class AddAttachmentNameToAttachments < ActiveRecord::Migration[5.2]
  def self.up
    change_table :attachments do |t|
      t.attachment :name
    end
  end

  def self.down
    remove_attachment :attachments, :name
  end
end
