User.seed do |f|
	@password_salt  = BCrypt::Engine.generate_salt
  f.id   					= 1
  f.email 			  = "admin"
  f.username 			  = "admin"
  f.phone 	      = "081321090490"
  f.password_salt = @password_salt
  f.password_hash = BCrypt::Engine.hash_secret('admin', @password_salt)
  f.is_admin 			= true
  f.is_active 		= true
end