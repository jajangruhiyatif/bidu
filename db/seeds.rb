# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# ProductBid.create(user_id: 1, name: 'android', image: '/var/www/img/a.jpg', price: 5000000, due_date: '12-09-2019')
# ProductBid.create(user_id: 1, name: 'panci', image: '/var/www/img/b.jpg', price: 300000, due_date: '12-10-2019')
# ProductBid.create(user_id: 1, name: 'permadani', image: '/var/www/img/c.jpg', price: 10000000, due_date: '12-11-2019')

AuctionItem.create(user_id: 1, category_id: 2, product_name: 'nippon swallow', product_price: 7500, img_path: '/var/img/swallow.jpg', start_date: '-12-10', end_time: '2020-01-12', is_approve: true, created_at: '2019-04-10', updated_at: '2019-04-10')